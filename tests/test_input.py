def test_input_rw(tmpdir):
    tmpfile = str(tmpdir.join('test.inp'))
    
    from orangutan import Input
    
    class MyInput(Input):
        SECTION = 'TEST'
        parama = 'a'
        param2 = 2
        param3 = 3.4
    
    inp = MyInput()
    inp.param2 = 2.01
    inp.write(tmpfile)
    del inp
    
    inp = MyInput()
    inp.read(tmpfile)
    
    assert inp.param2 == 2.01
