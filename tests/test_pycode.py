def test_pycode1():
    code = 'sin(2*pi)+2*t'
    
    from orangutan import PyCode
    rps = PyCode(code, 'testcode')
    assert rps.run(t=10) == 20

def test_pycode_multiline():
    code = """
a = 5
b = 3
c = a*b*t  # c is the return variable here
    """
    
    from orangutan import PyCode
    rps = PyCode(code, 'testcode', var_name='c')
    assert rps.run(t=10) == 150
