Orangutan
=========

FEniCS finite element solvers can be implemented as quite short Python scripts due to the expressivity of UFL and the rich functionality of dolfin.
There is however some functionality that you often find yourself adding to many of these scripts as you put them "in production".
Examples of such features are reading input files, logging, saving restart files etc.

The Orangutan Python package contains functionality that you can plug into your existing solver without much/any changes to the code and without adding many lines of code.
You can as an example log a parameter, such as the maximum Peclet number in a time step, to the console while running and it will also end up as a time series in your HDF5 restart file for ease of postprocessing.


Documentation
-------------

TODO


Copyright and license
---------------------

Ocellaris is licensed under the Apache 2.0 license. The author of Orangutan is Tormod Landet.
