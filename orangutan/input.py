# encoding: utf8
from __future__ import division
import os, ConfigParser, StringIO

class Input(object):
    """
    Subclass this class and add class attributes which defines default values
    for the input variables
    """
    SECTION = 'Input'
    
    def read(self, inp_file_name):
        """
        Read an input file on INI format with a [self.SECTION] section
        """
        assert os.path.isfile(inp_file_name)
        cp = ConfigParser.RawConfigParser()
        cp.optionxform = str
        cp.read([inp_file_name])
        self.read_config_parser(cp)
        
    def write(self, file_name):
        """
        Write the input to a file
        """
        s = str(self)
        with open(file_name, 'wt') as f:
            f.write(s)
    
    def read_string(self, s, file_name='<string>'):
        cp = ConfigParser.RawConfigParser()
        cp.optionxform = str
        fp = StringIO.StringIO(s)
        cp.readfp(fp, '<string>')
        self.read_config_parser(cp)
    
    def read_config_parser(self, cp):
        assert cp.has_section(self.SECTION)
        for name in cp.options(self.SECTION):
            assert hasattr(self, name), 'Input %r was given but is not a valid parameter' % name
            value = cp.get(self.SECTION, name)
            try:
                value = eval(value)
            except:
                pass
            setattr(self, name, value)
    
    def __str__(self):
        cp = ConfigParser.RawConfigParser()
        cp.optionxform = str
        cp.add_section(self.SECTION)
        for name in sorted(dir(self)):
            if name[0] == '_' or name == 'SECTION':
                continue
            tvalue = getattr(type(self), name)
            value = getattr(self, name)
            if hasattr(value, '__call__') or isinstance(tvalue, property):
                continue
            cp.set(self.SECTION, name, value)
        fp = StringIO.StringIO()
        cp.write(fp)
        fp.seek(0)
        return fp.read()
