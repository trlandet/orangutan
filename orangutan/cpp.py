# encoding: utf8
from __future__ import division
import os, time
import dolfin as df

def load_cpp_module(header_files, source_files, cpp_dir, force_recompile=False):
    """
    Use the dolfin machinery to compile, wrap with swig and load a c++ module
    """
    header_sources = []
    for hpp_filename in header_files:
        hpp_filename = os.path.join(cpp_dir, hpp_filename)
        
        with open(hpp_filename, 'rt') as f:
            hpp_code = f.read()
        header_sources.append(hpp_code)
    
    # Force recompilation
    if force_recompile:
        header_sources.append('// %s \n' % time.time())
    
    try:
        module = df.compile_extension_module(code='\n\n\n'.join(header_sources),
                                             source_directory=cpp_dir, 
                                             sources=source_files,
                                             include_dirs=[cpp_dir])
    except RuntimeError, e:
        COMPILE_ERROR = "In instant.recompile: The module did not compile with command 'make VERBOSE=1', see "
        if e.message.startswith(COMPILE_ERROR):
            # Get the path of the error file
            path = e.message.split("'")[-2]
            # Print the error file
            with open(path, 'rt') as error:
                print error.read()
            raise
        
    return module
