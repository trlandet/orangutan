# encoding: utf8
from __future__ import division
import os
import numpy
import dolfin as df


class XDMFSaver(object):
    def __init__(self, funcs, prefix='results', disabled=False):
        """
        Small helper class to save fields to XDFM file format
        for visualisation in Paraview or similar programs
        """
        self.disabled = disabled
        if self.disabled:
            return
        self.funcs = funcs
        
        # Find unused file name for saving results
        result_file = '%s.xdmf' % prefix
        i = 1
        while os.path.isfile(result_file):
            i += 1
            result_file = '%s_%d.xdmf' % (prefix, i)
        
        # Open file where results will be stored
        self.xdmf = df.XDMFFile(df.mpi_comm_world(), result_file)

    def save(self, t):
        """
        Save the functions at the given time step
        """
        if self.disabled:
            return
        
        # Write functions to the XDMF file
        for func in self.funcs:
            self.xdmf.write(func, t)


def save_restart_file(h5_file_name, inp, log, mesh, funcs, t, it):
    """
    Save the data needed to restart the simulations to the given h5 file.
    The file will be overwritten if it exists
    """
    if os.path.isfile(h5_file_name):
        os.remove(h5_file_name)
    h5 = df.HDF5File(df.mpi_comm_world(), h5_file_name, 'w')
    
    # Write mesh
    h5.write(mesh, '/mesh')
    
    # Write FEniCS functions
    funcnames = []
    for func in funcs:        
        name = func.name()
        assert '/' not in name
        h5.write(func, '/%s' % name)
        
        # Save function names in a separate HDF attribute due to inability to 
        # list existing HDF groups when using the dolfin HDF5Function wrapper 
        assert ',' not in name
        funcnames.append(name)
    
    # Metadata
    tinfo = numpy.array([t, it, inp.dt])
    h5.write(tinfo, '/metadata/time_info')
    h5.attributes('/metadata')['restart_file_format'] = 1
    h5.attributes('/metadata')['functions'] = ','.join(funcnames)
    h5.attributes('/metadata')['input'] = str(inp)
    
    # Reports
    if log.reports:
        for rep_name, values in log.reports.items():
            assert ',' not in rep_name
            values = numpy.array(values, dtype=float)
            h5.write(values, '/reports/%s' % rep_name)
        h5.attributes('/reports')['report_names'] = ','.join(log.reports)
    
    h5.close()
    

def load_restart_file(h5_file_name, funcs, log):
    """
    Save the data needed to restart the simulations to the given h5 file.
    The file will be overwritten if it exists
    """
    import h5py
    
    if not os.path.isfile(h5_file_name):
        raise OSError('Restart file not found: %r' % h5_file_name)
    h5 = df.HDF5File(df.mpi_comm_world(), h5_file_name, 'r')
    
    # Read FEniCS functions
    for func in funcs:        
        name = func.name()
        assert '/' not in name
        h5.read(func, '/%s' % name)
    
    # Report names
    report_names = h5.attributes('/reports')['report_names'].split(',')
    h5.close()
    
    ###########################################
    # End of df.HDF5File -- start of h5py
    # We use h5py since dolfin cannot load
    # numpy arrays from hdf5
    hdf = h5py.File(h5_file_name, 'r')
    
    # Metadata
    t, it = hdf['/metadata/time_info'][:2]
    it = int(it)
    
    # Report time series
    for rep_name in report_names:
        ts = list(hdf['/reports'][rep_name])
        log.reports[rep_name] = ts
    
    return t, it
