# encoding: utf8
from __future__ import division
import sys, collections, contextlib, time
from . import RANK


RED = '\033[91m%s\033[0m'    # ANSI escape code Bright Red
YELLOW = '\033[93m%s\033[0m' # ANSI escape code Bright Yellow


class Log(object):
    def __init__(self, logfile=None, console=True):
        """
        The Log lets you print messages to a file and to the 
        console at the same time. Errors will be written to
        console in red and warnings in yellow. The text "ERROR: "
        and "WARNING: " will be prepended to each line of the
        respective type of messages. 
        """
        self.files = []
        if logfile:
            self.files.append(open(logfile, 'wt'))
        if console:
            self.files.append(sys.stdout)
        self.reports = collections.OrderedDict()
        self.report_formats = {}
        self._the_log = []
        
    def write(self, message, color=None):
        if RANK == 0:
            for f in self.files:
                if color is not None and f.fileno() == 1:
                    f.write(color % message)
                else:
                    f.write(message)
        self._the_log.append(message)
    
    def info(self, message):
        self.write(message)
    
    def warning(self, message):
        message_warn = message[:-1].replace('\n', '\nWARNING: ')
        message = 'WARNING: %s%s' % (message_warn, message[-1])
        self.write(message, YELLOW)
    
    def error(self, message):
        message_err = message[:-1].replace('\n', '\nERROR: ')
        message = 'ERROR: %s%s' % (message_err, message[-1])
        self.write(message, RED)
    
    def dump_object(self, obj, prefix=''):
        for name in sorted(dir(obj)):
            if name[0] == '_':
                continue
            tvalue = getattr(type(obj), name)
            value = getattr(obj, name)
            if hasattr(value, '__call__') or isinstance(tvalue, property):
                continue
            self.info('%s%s = %r\n' % (prefix, name, value))
    
    def flush(self):
        for f in self.files:
            if f.fileno() != 1:
                f.flush()
    
    def get_full_log(self):
        return ''.join(self._the_log)
    
    def report(self, rep_name, value, fmt='% .3e'):
        if not rep_name in self.reports:
            self.reports[rep_name] = []
        self.report_formats[rep_name] = fmt
        self.reports[rep_name].append(value)
    
    def report_timestep(self):
        tsrep = []
        for rep_name, values in self.reports.items():
            fmt = self.report_formats[rep_name]
            if fmt:
                try:
                    tsrep.append(('%s: ' + fmt) % (rep_name, values[-1]))
                except:
                    print fmt, rep_name, values[-1]
                    raise
        self.info('  '.join(tsrep) + '\n')
    
    @contextlib.contextmanager
    def timer(self, name):
        t_start = time.time()
        yield
        duration = time.time() - t_start
        self.report(name, duration, '%4.2fs')
