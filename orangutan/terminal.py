# encoding: utf8
from __future__ import division
import sys
from . import NCPUS, RANK


def should_stop():
    """
    Return True if requested to stop (user pressed s+Enter)
    """
    # Check if the user has written something on stdin for us
    if RANK == 0:
        commands = get_input_from_terminal()
    else:
        commands = []
    
    # Make sure all processes get the same commands
    if NCPUS > 1:
        from mpi4py.MPI import COMM_WORLD as comm
        commands = comm.bcast(commands)
    
    for command in commands:
        if command == 's':
            return True
    
    return False


def get_input_from_terminal():
    """
    Read stdin to see if there are some commands for us to execute
    """
    # The select() system call does not work on windows
    if not sys.__stdin__.isatty() or 'win' in sys.platform:
        return []
    
    import select
    
    commands = []
    has_input = lambda: sys.stdin in select.select([sys.stdin], [], [], 0)[0]
    
    # Check if there is input on stdin and read it if it exists
    while has_input(): 
        line = sys.stdin.readline()
        command = line.strip().lower()
        commands.append(command)
    
    return commands
