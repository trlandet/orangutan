# encoding: utf8
from __future__ import division
import re
import numpy as np, dolfin as df
from math import *

class PyCode(object):
    def __init__(self, code_string, description, var_name=None):
        """
        This class runs a string as Python code
        
        It the code contains a newline it must be the core
        of a function and is run with exec() otherwise it is
        assumed to be an expression and is run with eval()
        
        If varname is specified then any multiline code block
        must define this variable
        """
        if isinstance(code_string, (float, int, long, complex)):
            code_string = repr(code_string)
        
        self.description = description
        self.var_name = var_name
        needs_exec = self._validate_code(code_string)
        
        filename = '<python-string %s>' % description
        self.code = compile(code_string, filename, 'exec' if needs_exec else 'eval')
        self.needs_exec = needs_exec
    
    def _validate_code(self, code_string):
        """
        Check that the code is either a single expression or a valid
        multiline expression that defines the variable varname 
        """
        # Does this code define the variable, var_name = ...
        # or assign to an element, var_name[i] = ... ?
        if self.var_name is not None:
            vardef = r'.*(^|\s)%s(\[\w\])?\s*=' % self.var_name
            has_vardef = re.search(vardef, code_string) is not None
        else:
            has_vardef = False
        
        multiline = '\n' in code_string
        needs_exec = multiline or has_vardef
        
        if needs_exec and self.var_name is not None and not has_vardef:
            raise ValueError('Invalid: %s\n' % self.description +
                             'Multi line expression must define the variable "%s"'
                             % self.var_name)
        
        return needs_exec
    
    def run(self, **kwargs):
        """
        Run the code
        """
        # Make sure the keyword arguments accessible
        locals().update(kwargs)
        
        if self.needs_exec:
            exec(self.code)
            if self.var_name is not None:
                # The code defined a variable. Return it
                return locals()[self.var_name]
            else:
                # No return value
                return
        else:
            # Return the result of evaluating the expression
            return eval(self.code)
