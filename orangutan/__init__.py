# encoding: utf8
from __future__ import division

import dolfin as df
NCPUS = df.MPI.size(df.mpi_comm_world())
RANK = df.MPI.rank(df.mpi_comm_world())

from .input import Input
from .log import Log
from .io import XDMFSaver, save_restart_file, load_restart_file
from .cpp import load_cpp_module
from .terminal import should_stop, get_input_from_terminal
from .pycode import PyCode
